# Comments
# Comments in Python are done using the "#" symbol
# ctrl + / - comments in Python - one line comment

"""
although we have no keybind for multi-line comment, it is still possible through the
use of 2 sets of double qoutation marks
"""

# Python Syntax
# Hello World in Python
print("Hello World")

# Indentation
# Where in other programming languages the indentation in code
# is for readability, the indentation is Python is VERY IMPORTANT.
